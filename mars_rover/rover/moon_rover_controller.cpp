//
// Created by yonggang zhao on 2022/8/7.
//

#include "moon_rover_controller.h"
#include "rover_lower_api.h"

void MoonRoverController::turnLeft() {
    moon_rover_turn_left();
    moon_rover_turn_left();
}
void MoonRoverController::turnRight() {
    moon_rover_turn_right();
    moon_rover_turn_right();
}

void MoonRoverController::move() {
    moon_rover_move();
    moon_rover_move();
};