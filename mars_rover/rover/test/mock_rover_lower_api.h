//
// Created by yonggang zhao on 2022/8/8.
//

#ifndef UNIT_TEST_MOCK_ROVER_LOWER_API_H
#define UNIT_TEST_MOCK_ROVER_LOWER_API_H

#include <gmock-free/gmock-free.h>
#include "rover_lower_api.h"

DECL_MOCK_FREE_FUNC(void, mars_rover_move, ());
DECL_MOCK_FREE_FUNC(void, mars_rover_turn_left, ());
DECL_MOCK_FREE_FUNC(void, mars_rover_turn_right, ());
DECL_MOCK_FREE_FUNC(void, moon_rover_move, ());
DECL_MOCK_FREE_FUNC(void, moon_rover_turn_left, ());
DECL_MOCK_FREE_FUNC(void, moon_rover_turn_right, ());

#endif //UNIT_TEST_MOCK_ROVER_LOWER_API_H
