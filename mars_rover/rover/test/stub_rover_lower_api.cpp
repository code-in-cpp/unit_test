//
// Created by yonggang zhao on 2022/8/8.
//

#include "rover_lower_api.h"
#include <iostream>

void mars_rover_move() {
    std::cout << __FUNCTION__ << std::endl;
}
void mars_rover_turn_left() {
    std::cout << __FUNCTION__ << std::endl;
}
void mars_rover_turn_right() {
    std::cout << __FUNCTION__ << std::endl;
}
void moon_rover_move() {
    std::cout << __FUNCTION__ << std::endl;
}
void moon_rover_turn_left(){
    std::cout << __FUNCTION__ << std::endl;
}
void moon_rover_turn_right(){
    std::cout << __FUNCTION__ << std::endl;
}