//
// Created by yonggang zhao on 2022/8/8.
//

#include "mock_rover_lower_api.h"
#include "mars_rover_controller.h"

TEST(MarsRoverControllerTest, move_controller_should_mars_rover_move) {
    // arrange:
    MarsRoverController controller;

    // assert:
    EXPECT_FREE_CALL(mars_rover_move).Times(1);

    // act:
    controller.move();
}

TEST(MarsRoverControllerTest, turnLeft_controller_should_mars_rover_turn_left) {
    // arrange:
    MarsRoverController controller;

    // assert:
    EXPECT_FREE_CALL(mars_rover_turn_left).Times(1);

    // act:
    controller.turnLeft();
}

TEST(MarsRoverControllerTest, turnLeft_controller_should_mars_rover_turn_right) {
    // arrange:
    MarsRoverController controller;

    // assert:
    EXPECT_FREE_CALL(mars_rover_turn_right).Times(1);

    // act:
    controller.turnRight();
}