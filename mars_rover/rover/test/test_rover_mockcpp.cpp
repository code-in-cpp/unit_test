//
// Created by yonggang zhao on 2022/8/7.
//

#include "rover.h"
#include "control_center.h"
#include <gmock/gmock.h>
#include <mockcpp/mokc.h>

using testing::Test;

class MockRoverController : public RoverController {
public:
    MOCK_METHOD(void, turnLeft, (), (override));
    MOCK_METHOD(void, turnRight, (), (override));
    MOCK_METHOD(void, move, (), (override));
};

class RoverTest : public Test {
public:
    void SetUp() override {
    }
    void TearDown() override {
        GlobalMockObject::verify();
    }

    void ShouldMoveToLocation(int x, int y) {
        EXPECT_CALL(roverController, move).Times(testing::Exactly(1));

        // assert: control center will recv new location
        MOCKER(cc_current_location).expects(once()).with(eq(x), eq(y)).will(ignoreReturnValue());
    }
protected:
    std::unique_ptr<Rover> rover;
    Marker marker;
    MockRoverController roverController;
};

TEST_F(RoverTest, move_in_0_0_north_should_notify_location_is_0_and_1_and_controller_recv_move)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::N());

    // assert: controller recv move
    EXPECT_CALL(roverController, move).Times(testing::Exactly(1));

    // act: give rover move command
    rover->move();

    // assert: control center will recv new location
    MOCKER(cc_current_location).expects(once()).with(eq(0), eq(1)).will(ignoreReturnValue());

    // act: 指令执行完毕
    rover->complete();
}

TEST_F(RoverTest, move_in_0_0_north_when_0_1_is_crash_should_ignore_command)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::N());
    marker.crashAt(0, 1);

    // assert: controller recv should not move
    EXPECT_CALL(roverController, move).Times(testing::Exactly(0));
    MOCKER(cc_current_location).expects(once()).with(eq(0), eq(0));

    // act: give rover move command
    rover->move();
    rover->complete();
}

TEST_F(RoverTest, turn_left_and_move_0_0_east_should_notify_location_is_0_and_1_and_controller_recv_move)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::E());

    // assert:
    EXPECT_CALL(roverController, turnLeft).Times(1);

    // assert: controller recv move
    EXPECT_CALL(roverController, move).Times(testing::Exactly(1));

    // assert: control center will recv new location
    MOCKER(cc_current_location).expects(once()).with(eq(0), eq(1)).will(ignoreReturnValue());

    // act: give rover move command
    rover->turnLeft();
    rover->move();
    rover->complete();
}

TEST_F(RoverTest, turn_left_when_east_should_call_turn_left)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::E());

    // assert:
    EXPECT_CALL(roverController, turnLeft).Times(1);

    // act: give rover move command
    rover->turnLeft();
}



TEST_F(RoverTest, refactored_turn_left_and_move_0_0_east_should_notify_location_is_0_and_1_and_controller_recv_move)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::E());
    rover->turnLeft();

    // assert: 应该移动到新位置（0， 1）
    ShouldMoveToLocation(0, 1);

    // act: give rover move command
    rover->move();
    rover->complete();
}

TEST_F(RoverTest, turn_right_when_west_should_call_turn_right)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::W());

    // assert:
    EXPECT_CALL(roverController, turnRight).Times(1);

    // act: give rover move command
    rover->turnRight();
}

TEST_F(RoverTest, turn_right_and_move_when_west_should_move_success)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::W());
    rover->turnRight();

    // assert: 应该移动到新位置（0， 1）
    ShouldMoveToLocation(0, 1);

    // act: give rover move command
    rover->move();
    rover->complete();
}

TEST_F(RoverTest, turn_around_when_south_should_call_turn_around)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::S());

    // assert:
    EXPECT_CALL(roverController, turnRight).Times(2);

    // act: give rover move command
    rover->turnAround();
}

TEST_F(RoverTest, turn_around_and_move_when_south_should_move_success)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::S());
    rover->turnAround();

    // assert: 应该移动到新位置（0， 1）
    ShouldMoveToLocation(0, 1);

    // act: give rover move command
    rover->move();
    rover->complete();
}

TEST_F(RoverTest, crash_at_new_location_should_not_movable)
{
    // arrange: rover placed on 0 0 N
    rover = std::make_unique<Rover>(marker, roverController, 0, 0, Orientation::N());
    rover->move();

    // act: give rover move command
    rover->notifyCrash();

    // act: 0, 1 should not be movable
    ASSERT_FALSE(marker.checkMovable(0, 1));
}
