//
// Created by yonggang zhao on 2022/8/7.
//

#include "control_center.h"
#include <iostream>

void cc_current_location(int x, int y) {
    std::cout << __FUNCTION__  << " x" << x << " " << "y" << y << std::endl;
}

void cc_ignore_command() {
    std::cout << __FUNCTION__ << std::endl;
}
