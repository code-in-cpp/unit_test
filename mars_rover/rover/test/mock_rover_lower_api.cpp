//
// Created by yonggang zhao on 2022/8/8.
//

#include "mock_rover_lower_api.h"

MOCK_FREE_FUNC(void, mars_rover_move, ());
MOCK_FREE_FUNC(void, mars_rover_turn_left, ());
MOCK_FREE_FUNC(void, mars_rover_turn_right, ());
MOCK_FREE_FUNC(void, moon_rover_move, ());
MOCK_FREE_FUNC(void, moon_rover_turn_left, ());
MOCK_FREE_FUNC(void, moon_rover_turn_right, ());