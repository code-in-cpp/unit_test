//
// Created by yonggang zhao on 2022/8/8.
//

#include "mock_rover_lower_api.h"
#include "moon_rover_controller.h"

TEST(MoonRoverControllerTest, move_controller_should_moon_rover_move) {
    // arrange:
    MoonRoverController controller;

    // assert:
    EXPECT_FREE_CALL(moon_rover_move).Times(2);

    // act:
    controller.move();
}

TEST(MoonRoverControllerTest, turnLeft_controller_should_moon_rover_turn_left) {
    // arrange:
    MoonRoverController controller;

    // assert:
    EXPECT_FREE_CALL(moon_rover_turn_left).Times(2);

    // act:
    controller.turnLeft();
}

TEST(MoonRoverControllerTest, turnLeft_controller_should_moon_rover_turn_right) {
    // arrange:
    MoonRoverController controller;

    // assert:
    EXPECT_FREE_CALL(moon_rover_turn_right).Times(2);

    // act:
    controller.turnRight();
}