//
// Created by yonggang zhao on 2022/8/7.
//

#include "mars_rover_controller.h"
#include "rover_lower_api.h"

void MarsRoverController::turnLeft()
{
    mars_rover_turn_left();
}

void MarsRoverController::turnRight()
{
    mars_rover_turn_right();
}

void MarsRoverController::move()
{
    mars_rover_move();
}