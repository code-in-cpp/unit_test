//
// Created by yonggang zhao on 2022/8/6.
//

#ifndef UNIT_TEST_ROVER_H
#define UNIT_TEST_ROVER_H

#include "orientation.h"
#include "marker.h"
#include "rover_controller.h"
#include "executor.h"

class Rover : public Executor{
public:
    Rover(Marker& marker, RoverController& controller, int x, int y, Orientation& orientation);
    void turnLeft() override;
    void turnRight() override;
    void turnAround() override;
    void move() override;
    void complete() override;
    void notifyCrash();

private:
    Marker& marker;
    RoverController& controller;
    int x{};
    int y{};
    Orientation orientation;
};

#endif //UNIT_TEST_ROVER_H
