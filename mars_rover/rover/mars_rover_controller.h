//
// Created by yonggang zhao on 2022/8/7.
//

#ifndef UNIT_TEST_MARS_ROVER_CONTROLLER_H
#define UNIT_TEST_MARS_ROVER_CONTROLLER_H
#include "rover_controller.h"

class MarsRoverController : public RoverController{
public:
    void turnLeft() override;
    void turnRight() override;
    void move() override;
};


#endif //UNIT_TEST_MARS_ROVER_CONTROLLER_H
