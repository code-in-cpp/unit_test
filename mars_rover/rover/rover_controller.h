//
// Created by yonggang zhao on 2022/8/7.
//

#ifndef UNIT_TEST_ROVER_CONTROLLER_H
#define UNIT_TEST_ROVER_CONTROLLER_H

class RoverController {
public:
    virtual void turnLeft() = 0;
    virtual void turnRight() = 0;
    virtual void move() = 0;
    virtual ~RoverController() = default;
};

#endif //UNIT_TEST_ROVER_CONTROLLER_H
