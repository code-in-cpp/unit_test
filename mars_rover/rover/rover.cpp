//
// Created by yonggang zhao on 2022/8/6.
//

#include "rover.h"
#include "control_center.h"

Rover::Rover(Marker &marker, RoverController &controller, int x, int y, Orientation &orientation) :
    marker(marker),
    controller(controller),
    x(x),
    y(y),
    orientation(orientation) {
}

void Rover::move() {
    auto [newX, newY] = orientation.move(x, y);
    if (marker.checkMovable(newX, newY)) {
        controller.move();
        x = newX;
        y = newY;
    }
}

void Rover::complete() {
    cc_current_location(x, y);
}

void Rover::turnLeft() {
    orientation = orientation.turnLeft();
    controller.turnLeft();
}
void Rover::turnRight() {
    orientation = orientation.turnRight();
    controller.turnRight();
}

void Rover::turnAround() {
    this->turnRight();
    this->turnRight();
}

void Rover::notifyCrash() {
    marker.crashAt(x, y);
}