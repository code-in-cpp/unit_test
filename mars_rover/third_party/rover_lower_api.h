//
// Created by yonggang zhao on 2022/8/7.
//

#ifndef UNIT_TEST_ROVER_LOWER_API_H
#define UNIT_TEST_ROVER_LOWER_API_H

#ifdef __cplusplus
extern "C" {
#endif
void mars_rover_move();
void mars_rover_turn_left();
void mars_rover_turn_right();
void moon_rover_move();
void moon_rover_turn_left();
void moon_rover_turn_right();
#ifdef __cplusplus
}
#endif

#endif //UNIT_TEST_ROVER_LOWER_API_H
