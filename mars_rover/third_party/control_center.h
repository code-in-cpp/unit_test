//
// Created by yonggang zhao on 2022/8/7.
//

#ifndef UNIT_TEST_CONTROL_CENTER_H
#define UNIT_TEST_CONTROL_CENTER_H

#ifdef __cplusplus
extern "C" {
#endif
void cc_current_location(int x, int y);
void cc_ignore_command();
#ifdef __cplusplus
}
#endif

#endif //UNIT_TEST_CONTROL_CENTER_H
