//
// Created by yonggang zhao on 2022/8/6.
//

#include "marker.h"
#include <gmock/gmock.h>

using testing::Test;

class MarkerTest : public Test {
protected:
    Marker marker;
};

TEST_F(MarkerTest, no_rover_crash_should_pass_any_location) {
    ASSERT_TRUE(marker.checkMovable(1, 2));
}

TEST_F(MarkerTest, where_rover_crash_should_not_pass) {
    // arrange
    marker.crashAt(1, 2);

    // assert
    ASSERT_FALSE(marker.checkMovable(1, 2));
}

TEST_F(MarkerTest, pass_where_rover_not_crash_should_pass) {
    // arrange
    marker.crashAt(1, 2);

    // assert
    ASSERT_TRUE(marker.checkMovable(1, 3));
}

TEST_F(MarkerTest, pass_where_rover_both_crash_should_pass) {
    // arrange
    marker.crashAt(1, 2);
    marker.crashAt(1, 3);

    // assert
    ASSERT_FALSE(marker.checkMovable(1, 3));
}