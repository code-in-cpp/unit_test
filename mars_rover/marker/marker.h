//
// Created by yonggang zhao on 2022/8/6.
//

#ifndef UNIT_TEST_MARKER_H
#define UNIT_TEST_MARKER_H
#include <vector>
#include <unordered_set>
#include <unordered_map>

class Marker {
public:
    bool checkMovable(int x, int y) const;
    void crashAt(int x, int y);
    static Marker& Instance();
private:
    std::unordered_map<int, std::unordered_set<int>> markers{};
};




#endif //UNIT_TEST_MARKER_H
