//
// Created by yonggang zhao on 2022/8/6.
//

#include "marker.h"

bool Marker::checkMovable(int x, int y) const{
    auto y_list_it = markers.find(x);
    if (y_list_it == markers.end()) {
        return true;
    }

    auto y_it = y_list_it->second.find(y);
    if (y_it == y_list_it->second.end()) {
        return true;
    }

    return false;
}
void Marker::crashAt(int x, int y) {
    markers[x].emplace(y);
}