//
// Created by yonggang zhao on 2022/8/6.
//

#include "cmd_parser.h"

void CmdParser::execStrCommand(Executor& rover, std::string_view cmd) {
    for(auto c : cmd) {
        switch (c) {
            case 'L':
                rover.turnLeft();
                break;
            case 'R':
                rover.turnRight();
                break;
            case 'M':
                rover.move();
                break;
            default:
                break;
        }
    }

    rover.complete();
}