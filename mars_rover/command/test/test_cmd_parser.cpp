//
// Created by yonggang zhao on 2022/8/8.
//

#include "cmd_parser.h"
#include <gmock/gmock.h>

using testing::Expectation;

class MockExecutor : public Executor {
public:
    MOCK_METHOD(void, turnLeft, (), (override));
    MOCK_METHOD(void, turnRight, (), (override));
    MOCK_METHOD(void, turnAround, (), (override));
    MOCK_METHOD(void, move, (), (override));
    MOCK_METHOD(void, complete, (), (override));
};

class CmdParserTest : public testing::Test {
protected:
    MockExecutor executor;
};

TEST_F(CmdParserTest, L_should_turn_left_then_complete) {
    // assert:
    Expectation turnLeftExpected = EXPECT_CALL(executor, turnLeft()).Times(1);
    EXPECT_CALL(executor, complete()).Times(1).After(turnLeftExpected);

    // act
    CmdParser::execStrCommand(executor, "L");
}

TEST_F(CmdParserTest, R_should_turn_right_then_complete) {
    // assert:
    Expectation turnRightExpected = EXPECT_CALL(executor, turnRight()).Times(1);
    EXPECT_CALL(executor, complete()).Times(1).After(turnRightExpected);

    // act
    CmdParser::execStrCommand(executor, "R");
}

TEST_F(CmdParserTest, M_should_move_then_complete) {
    // assert:
    Expectation turnRightExpected = EXPECT_CALL(executor, move()).Times(1);
    EXPECT_CALL(executor, complete()).Times(1).After(turnRightExpected);

    // act
    CmdParser::execStrCommand(executor, "M");
}

TEST_F(CmdParserTest, A_should_do_nothing_then_complete) {
    // assert:
    EXPECT_CALL(executor, complete()).Times(1);

    // act
    CmdParser::execStrCommand(executor, "A");
}
