//
// Created by yonggang zhao on 2022/8/7.
//

#ifndef UNIT_TEST_EXECUTOR_H
#define UNIT_TEST_EXECUTOR_H

class Executor {
public:
    virtual void turnLeft() = 0 ;
    virtual void turnRight() = 0;
    virtual void turnAround() = 0 ;
    virtual void move() = 0;
    virtual void complete() = 0;
    virtual ~Executor() = default;
};

#endif //UNIT_TEST_EXECUTOR_H
