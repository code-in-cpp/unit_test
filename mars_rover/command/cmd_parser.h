
//
// Created by yonggang zhao on 2022/8/6.
//

#ifndef UNIT_TEST_CMDPARSER_H
#define UNIT_TEST_CMDPARSER_H

#include "executor.h"
#include <string>

class CmdParser {
public:
    static void execStrCommand(Executor& rover, std::string_view cmd);
};


#endif //UNIT_TEST_CMDPARSER_H
