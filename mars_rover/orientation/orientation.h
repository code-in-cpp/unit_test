//
// Created by yonggang zhao on 2022/8/6.
//

#ifndef UNIT_TEST_ORIENTATION_H
#define UNIT_TEST_ORIENTATION_H
#include <stdint.h>
#include <iostream>
#include <tuple>

class Orientation {
public:
    static Orientation& N();
    static Orientation& E();
    static Orientation& S();
    static Orientation& W();

    Orientation turnLeft();
    Orientation turnRight();
    std::tuple<int, int> move(int x, int y);
    friend bool operator==(const Orientation& lhs, const Orientation& rhs);

private:
    Orientation(uint8_t orientation);

    uint8_t orientation;
};

bool operator!=(const Orientation& lhs, const Orientation& rhs);
std::ostream& operator<<(std::ostream& os, const Orientation& orientation);

#endif //UNIT_TEST_ORIENTATION_H
