//
// Created by yonggang zhao on 2022/8/6.
//

#include "orientation.h"

Orientation& Orientation::N() {
    static Orientation north{3};
    return north;
}

Orientation& Orientation::W() {
    static Orientation west{2};
    return west;
}

Orientation& Orientation::S() {
    static Orientation south{1};
    return south;
}

Orientation& Orientation::E() {
    static Orientation east{0};
    return east;
}

static constexpr uint8_t ORIENTATION_COUNT = 4;

Orientation::Orientation(uint8_t orientation) : orientation(orientation%ORIENTATION_COUNT) {
}

Orientation Orientation::turnLeft() {
    if(*this == Orientation::E()) {
        return Orientation::N();
    }
    return Orientation{--orientation};
}

Orientation Orientation::turnRight() {
    if(*this == Orientation::N()) {
        return Orientation::E();
    }
    return Orientation{++orientation};
}

std::tuple<int, int> Orientation::move(int x, int y) {
    if (*this == Orientation::N()) {
        y++;
    } else if (*this == Orientation::E()) {
        x++;
    } else if (*this == Orientation::S()) {
        y--;
    } else if (*this == Orientation::W()) {
        x--;
    }

    return {x, y};
}

bool operator==(const Orientation& lhs, const Orientation& rhs) {
    return lhs.orientation == rhs.orientation;
}

bool operator!=(const Orientation& lhs, const Orientation& rhs) {
    return !(lhs == rhs);
}

std::ostream& operator<<(std::ostream& os, const Orientation& orientation) {
    if(orientation == Orientation::N()) {
        os << "North";
    } else if(orientation == Orientation::W()) {
        os << "West";
    } else if(orientation == Orientation::S()) {
        os << "South";
    } else {
        os << "East";
    }
    return os;
}