//
// Created by yonggang zhao on 2022/8/6.
//
#include <gmock/gmock.h>

#include "orientation.h"

using testing::TestWithParam;
using testing::Values;
using std::tuple;

class OrientationTurnTest : public TestWithParam<tuple<Orientation, Orientation>> {
};

TEST_P(OrientationTurnTest, turn_left_should_success)
{
    auto [expected_left, right] = GetParam();
    auto actual_left = right.turnLeft();
    ASSERT_EQ(actual_left, actual_left);
}

TEST_P(OrientationTurnTest, turn_right_should_success)
{
    auto [left, expected_right] = GetParam();
    auto actual_right = left.turnRight();
    ASSERT_EQ(expected_right, actual_right);
}

INSTANTIATE_TEST_SUITE_P(left_right_data, OrientationTurnTest,
                         Values(std::tuple(Orientation::N(), Orientation::E()),
                                std::tuple(Orientation::E(), Orientation::S()),
                                std::tuple(Orientation::S(), Orientation::W()),
                                std::tuple(Orientation::W(), Orientation::N())));

using OldX = int;
using OldY = int;
using NewX = int;
using NewY = int;
class OrientationMoveTest : public TestWithParam<tuple<Orientation, OldX, OldY, NewX, NewY>> {
};

TEST_P(OrientationMoveTest, turn_right_should_success)
{
    auto [orientation, oldX, oldY, expectedNewX, expectedNewY] = GetParam();
    auto [actualNewX, actualNewY] = orientation.move(oldX, oldY);
    ASSERT_EQ(actualNewX, expectedNewX);
    ASSERT_EQ(actualNewY, expectedNewY);
}

INSTANTIATE_TEST_SUITE_P(move, OrientationMoveTest,
                         Values(tuple(Orientation::N(), OldX{0}, OldY{0}, NewX{0}, NewY{1}),
                                tuple(Orientation::E(), OldX{0}, OldY{0}, NewX{1}, NewY{0}),
                                tuple(Orientation::S(), OldX{0}, OldY{0}, NewX{0}, NewY{-1}),
                                tuple(Orientation::W(), OldX{0}, OldY{0}, NewX{-1}, NewY{0})
                                )
                        );



