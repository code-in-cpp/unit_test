#include <unordered_map>
#include <string>
#include <stdexcept>
#include <fmt/format.h>
#include <gmock/gmock.h>

class FileSystem {
 public:
  virtual void writeFile(std::string filename, std::string content) = 0;
  virtual std::string readFile(std::string filename) = 0;
  virtual ~FileSystem() = default;
};

class FakeFileSystem : public FileSystem {
  std::unordered_map<std::string, std::string> files;
 public:
  void writeFile(std::string filename, std::string content) override {
    // 添加文件名和内容到map
    files.emplace(filename, content);
  }

  std::string readFile(std::string filename) override {
    auto it = files.find(filename);
    if(it == files.end()) {
      throw std::invalid_argument(fmt::format("invalid file name {} by readFile", filename));
    }
    return it->second;
  }
};

using ::testing::Eq;
TEST(FakeFileSystemTest, shouldWriteFileSuccess) {
  // Given: FileSystem is initial
  FakeFileSystem fileSystem;

  // When: write a file with content
  fileSystem.writeFile("file", "content");

  // Then: content of file can be read
  ASSERT_THAT(fileSystem.readFile("file"), Eq(std::string("content")));
}