project(stubbing_example)

add_executable(${PROJECT_NAME} test_stubbing.cpp)
target_link_libraries(${PROJECT_NAME}
        PRIVATE
        gmock
        gmock_main
        fmt)

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME}
        XML_OUTPUT_DIR report
        )

add_executable(faking_filesystem faking_filesystem.cpp)
target_link_libraries(faking_filesystem
        PRIVATE
        gmock
        gmock_main
        fmt)

include(GoogleTest)
gtest_discover_tests(faking_filesystem
        XML_OUTPUT_DIR report
        )