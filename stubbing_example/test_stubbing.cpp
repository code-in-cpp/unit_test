#include <gmock/gmock.h>

using ::testing::Return;

struct User {
  int user_id;
};

class AuthorizationService {
 public:
  virtual User* lookupUser(int user_id) = 0;
  virtual ~AuthorizationService() = default;
};

class AccessManager {
  AuthorizationService* authorizationService;

 public:
  AccessManager(AuthorizationService* authorizationService)
      : authorizationService(authorizationService) {}

  bool userHasAccess(int user_id) const {
    return authorizationService->lookupUser(user_id) != nullptr;
  }
};

class MockAuthorizationService : public AuthorizationService {
 public:
  MOCK_METHOD(User*, lookupUser, (int user_id), (override));
};

TEST(AccessManagerTest, shouldCheckUserHasAccessOrNot) {
  // 传入由模拟框架创建的测试替身
  MockAuthorizationService mockAuthorizationService;
  AccessManager accessManager(&mockAuthorizationService);

  // 如果返回值是null，则该用户ID没有访问权限
  ON_CALL(mockAuthorizationService, lookupUser(5)).WillByDefault(Return(nullptr));
  ASSERT_FALSE(accessManager.userHasAccess(5));

  // 如果返回值为非null，则该用户ID应该有访问权限
  User testUser;
  ON_CALL(mockAuthorizationService, lookupUser(6)).WillByDefault(Return(&testUser));
  ASSERT_TRUE(accessManager.userHasAccess(6));
}

TEST(AccessManagerTest, shouldCheckUserHasAccessOrNotByMock) {
  // 传入由模拟框架创建的测试替身
  MockAuthorizationService mockAuthorizationService;
  AccessManager accessManager(&mockAuthorizationService);

  // 如果返回值是null，则该用户ID没有访问权限
  EXPECT_CALL(mockAuthorizationService, lookupUser(5)).WillOnce(Return(nullptr));
  ASSERT_FALSE(accessManager.userHasAccess(5));

  // 如果返回值为非null，则该用户ID应该有访问权限
  User testUser;
  EXPECT_CALL(mockAuthorizationService, lookupUser(6)).WillOnce(Return(&testUser));
  ASSERT_TRUE(accessManager.userHasAccess(6));
}
