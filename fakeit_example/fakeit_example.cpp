#include <gtest/fakeit.hpp>
#include <gmock/gmock.h>

using ::testing::Return;

struct SomeInterface {
  virtual int foo(int) = 0;
  virtual int bar(std::string) = 0;
};

class MockSomeInterface : public SomeInterface {
 public:
  MOCK_METHOD(int, foo, (int), (override));
  MOCK_METHOD(int, bar, (std::string), (override));
};

TEST(MockDemo, should_mock_method_by_gmock) {
  MockSomeInterface mockSomeInterface;

  ON_CALL(mockSomeInterface, foo(5)).WillByDefault(Return(5));
  ASSERT_EQ(5, mockSomeInterface.foo(5));

  mockSomeInterface.foo(6);

  EXPECT_CALL(mockSomeInterface, bar("mock")).WillOnce(Return(6));
  ASSERT_EQ(6, mockSomeInterface.bar("mock"));

}
using fakeit::Mock;
using fakeit::When;
using fakeit::Verify;

TEST(MockDemo, should_mock_method_by_fakeit) {
  fakeit::Mock<SomeInterface> mock;

  When(Method(mock,foo).Using(5)).Return(5);

  SomeInterface& interface = mock.get();
  ASSERT_EQ(5, interface.foo(5));

  When(Method(mock,bar)).Return(6);
  ASSERT_EQ(interface.bar("mock"), 6);

  Verify(Method(mock, bar).Using("mock"));
}

using ::testing::NiceMock;
using ::testing::StrictMock;

TEST(MockDemo, mock_type_test) {
  MockSomeInterface naggy_mock;

  naggy_mock.foo(6);

  NiceMock<MockSomeInterface> nice_mock;

  nice_mock.foo(6);

  StrictMock<MockSomeInterface> strict_mock;

  strict_mock.foo(6);
}

#include <string>

struct Protocol {
  std::vector<int> port_list;
  std::string name;
};

MATCHER(IsHttpProtocol, "") {
  *result_listener << "where actual protoco is \"" << arg.name << "\"\n" ;
  *result_listener << " port list is [";
  bool check_port = true;
  for(auto port: arg.port_list) {
    *result_listener << port << " ";
    if(port != 80 && port != 8080 && !check_port)
      check_port = false;
  }
  *result_listener << "]";

  return arg.name == "http" && check_port;
}

TEST(MockDemo, test_customer_matcher) {
  Protocol protocl{{80, 8080}, "ip"};
  ASSERT_THAT(protocl, IsHttpProtocol());
}

using ::testing::_;
using ::testing::InSequence;

struct FooInterface {
  virtual void DoThis(int) = 0;
  virtual ~FooInterface() = default;
};

struct BarInterface {
  virtual void DoThat(int) = 0;
  virtual ~BarInterface() = default;
};

struct MockFoo : FooInterface {
  MOCK_METHOD(void, DoThis, (int), (override));
};

struct MockBar : BarInterface {
  MOCK_METHOD(void, DoThat, (int), (override));
};

TEST(MockDemo, test_sequence) {
  MockFoo foo;
  MockBar bar;

  {
    InSequence s;

    EXPECT_CALL(foo, DoThis(5));
    EXPECT_CALL(bar, DoThat(_))
        .Times(2);
    EXPECT_CALL(foo, DoThis(6));
  }

  foo.DoThis(6);
}



TEST(MockDemo, test_after_sequence) {
  MockFoo foo;
  MockBar bar;

  using ::testing::Expectation;
  Expectation expect_a = EXPECT_CALL(foo, DoThis(5));
  Expectation expect_b = EXPECT_CALL(bar, DoThat(_))
      .Times(2);
  EXPECT_CALL(foo, DoThis(6)).After(expect_a, expect_b);

  bar.DoThat(1);
  foo.DoThis(5);
  bar.DoThat(0);
  foo.DoThis(6);
}


TEST(MockDemo, test_dag_sequence) {
  MockFoo foo;
  MockBar bar;

  using ::testing::Sequence;
  Sequence s1, s2;
  EXPECT_CALL(foo, DoThis(5)).InSequence(s1);
  EXPECT_CALL(bar, DoThat(_))
      .Times(2).InSequence(s2);
  EXPECT_CALL(foo, DoThis(6)).InSequence(s1, s2);

  bar.DoThat(1);
  foo.DoThis(5);
  bar.DoThat(0);
  foo.DoThis(6);
}

