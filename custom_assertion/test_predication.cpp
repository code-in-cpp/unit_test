#include <gmock/gmock.h>

#include <algorithm>
#include <numeric>
using namespace std;

// function to check for gcd
bool MutuallyPrime(unsigned int a, unsigned int b)
{
    return (__gcd(a, b) == 1);
}

TEST(coprime, 10_and_3_should_be_coprime) {
    ASSERT_PRED2(MutuallyPrime, 10, 3);
}

// A predicate-formatter for asserting that two integers are mutually prime.
testing::AssertionResult AssertMutuallyPrime(const char* m_expr,
                                             const char* n_expr,
                                             unsigned int m,
                                             unsigned int n) {
    if (MutuallyPrime(m, n)) return testing::AssertionSuccess();

    return testing::AssertionFailure() << m_expr << " and " << n_expr
                                       << " (" << m << " and " << n << ") are not mutually prime, "
                                       << "as they have a common divisor " << __gcd(m, n);
}

TEST(coprime, 10_and_7_should_not_be_mutually_prime) {
    EXPECT_PRED_FORMAT2(AssertMutuallyPrime, 10, 7);
}

