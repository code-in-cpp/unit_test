
#include <cassert>
#include <iostream>
#include "rectangle.h"

int main() {

    std::cout << "Scenario1: perimeter of default rectangle should be zero" << std::endl;
    {
        std::cout << "given: a default rectangle" << std::endl;
        Rectangle rectangle;
        std::cout << "when: get perimeter of this rectangle" << std::endl;
        std::cout << "then: perimeter should be 0" << std::endl;
        assert(0 == rectangle.perimeter());
    }

    std::cout << "Scenario2: perimeter of rectangle with length and width should be 2 X (length + width)" << std::endl;
    {
        std::cout << "given: a rectangle with width {5} and height {8}" << std::endl;
        Rectangle rectangle{5, 8};
        std::cout << "when: get perimeter of this rectangle" << std::endl;
        std::cout << "then: perimeter should be 26" << std::endl;
        assert(26 == rectangle.perimeter());
    }

    std::cout << "Scenario3: perimeter of rectangle with invalid length or width should throw exception" << std::endl;
    {
        std::cout << "given: a rectangle with width {-5} and height {8}" << std::endl;
        try {
            Rectangle rectangle{-5, 8};
            assert(false);
        }
        catch(std::invalid_argument& e) {
            assert(true);
        }
        catch(...) {
            assert(false);
        }
    }
}