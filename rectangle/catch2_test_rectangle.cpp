#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "rectangle.h"

SCENARIO("perimeter of rectangle should be 2 X length + width") {
  GIVEN("a rectangle with {width} and {height}") {
    Rectangle rectangle{5, 8};
    WHEN("get perimeter of this rectangle") {
      auto perimeter = rectangle.perimeter();
      THEN("perimeter of this rectangle should equal {perimeter}") {
        REQUIRE(perimeter == 26);
      }
    }
    WHEN( "get area of this rectangle") {
      auto area = rectangle.area();
      THEN(" area of this rectangle should equal {area}") {
        REQUIRE(area == 40);
      }
    }
  }
}

