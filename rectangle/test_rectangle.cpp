//
// Created by yonggang zhao on 2021/5/24.
//

#include <gmock/gmock.h>

#include <tuple>
#include <vector>

#include "rectangle.h"

using ::testing::Eq;
// test cases
TEST(RectangleTest, perimeter_of_default_rectangle_should_be_zero) {
  Rectangle rectangle{};
  ASSERT_THAT(rectangle.perimeter(), Eq(0));
}

TEST(RectangleTest,
     perimeter_of_rectangle_with_length_and_width_should_be_2_X_length_width) {
  Rectangle rectangle{5, 8};
  ASSERT_THAT(rectangle.perimeter(), Eq(26));
}

TEST(
    RectangleTest,
    perimeter_of_rectangle_with_invalid_length_or_width_should_throw_exception) {
  ASSERT_THROW(Rectangle rectangle(-5, 8), std::invalid_argument);
}

class RectangleFunctionTest : public testing::Test {
 public:
  Rectangle rectangle{5, 8};
  void SetUp() override {}
  void TearDown() override {}
};

TEST_F(RectangleFunctionTest, rectangle_should_caculate_perimeter) {
  ASSERT_THAT(rectangle.perimeter(), Eq(26));
}

TEST_F(RectangleFunctionTest, rectangle_should_caculate_area) {
  ASSERT_THAT(rectangle.area(), Eq(40));
}

TEST(ParameterRectangleTest,
     perimeter_of_rectangle_with_length_and_width_should_be_2_X_length_width) {
  std::vector<std::tuple<std::int32_t, std::int32_t, std::int32_t>>
      test_parameter_list = {{5, 8, 26}, {1, 4, 10}, {10, 30, 80}};
  for (const auto& test_data : test_parameter_list) {
    SCOPED_TRACE(fmt::format("width: {}, height: {}, perimeter: {}",
                             std::get<0>(test_data), std::get<1>(test_data),
                             std::get<2>(test_data)));
    Rectangle rectangle{std::get<0>(test_data), std::get<1>(test_data)};
    ASSERT_THAT(rectangle.perimeter(), Eq(std::get<2>(test_data)));
  }
}

class RectangleTest
    : public ::testing::TestWithParam<
          std::tuple<std::int32_t, std::int32_t, std::int64_t>> {};

// Scenario1: perimeter of rectangle should be 2 X length + width
TEST_P(RectangleTest, perimeter_of_rectangle_should_be_2_X_length_width) {
  // Given: a rectangle with {width} and {height}
  auto test_data = GetParam();
  Rectangle rectangle{std::get<0>(test_data), std::get<1>(test_data)};

  // When: get perimeter of this rectangle
  // Then: perimeter of this rectangle should equal {perimeter}
  ASSERT_THAT(rectangle.perimeter(), Eq(std::get<2>(test_data)));
}

INSTANTIATE_TEST_SUITE_P(perimeter, RectangleTest,
                         ::testing::Values(std::tuple(5, 8, 26),
                                           std::tuple(1, 4, 10),
                                           std::tuple(10, 30, 80)));

class InvalidRectangleTest
    : public ::testing::TestWithParam<std::tuple<std::int32_t, std::int32_t>> {
};

TEST_P(
    InvalidRectangleTest,
    perimeter_of_rectangle_with_invalid_length_or_width_should_throw_exception) {
  auto test_data = GetParam();
  ASSERT_THROW(
      Rectangle rectangle(std::get<0>(test_data), std::get<1>(test_data)),
      std::invalid_argument);
}

INSTANTIATE_TEST_SUITE_P(invalid_width, InvalidRectangleTest,
                         ::testing::Combine(
                             ::testing::Range(-1000, -1, 100),
                             ::testing::Values(1, 0)
                             ));

INSTANTIATE_TEST_SUITE_P(invalid_height, InvalidRectangleTest,
                         ::testing::Combine(
                             ::testing::Values(1, 0),
                             ::testing::Range(-1000, -1, 100)
                         ));

