//
// Created by yonggang zhao on 2021/5/24.
//

#ifndef UNIT_TEST_RECTANGLE_H
#define UNIT_TEST_RECTANGLE_H
#include <cstdint>
#include <stdexcept>
#include <fmt/format.h>

class Rectangle {
public:
    Rectangle() = default;
    Rectangle(std::int32_t width, std::int32_t height) : width_(width), height_(height) {
        if(width_ < 0 || height_ < 0) {
            throw std::invalid_argument(fmt::format("width_{0} < 0 or height_{1} < 0", width_, height_));
        }
    }

    std::int64_t perimeter()const {
        return 2 * (width_ + height_);
    }

    std::int64_t area() const {
        return width_ * height_;
    }

private:
    std::int32_t width_ = 0;
    std::int32_t height_ = 0;
};


class Test {
 public:
  virtual ~Test();

 protected:
  // Sets up the test fixture.
  virtual void SetUp();

  // Tears down the test fixture.
  virtual void TearDown();

 private:
  virtual void TestBody() = 0;
};

#endif //UNIT_TEST_RECTANGLE_H
