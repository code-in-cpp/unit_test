#include <fmt/format.h>
#include <fmt/ranges.h>
#include <gmock/gmock.h>

#include "scored_document.h"

using namespace testing;

void AddScoredDocs(std::vector<ScoredDocument>& docs, int score) {
  ScoredDocument scoredDocument;
  scoredDocument.score = score;
  scoredDocument.doc = "test code";
  docs.emplace_back(scoredDocument);
}

TEST(sort_and_filter, Test_SortAndFilterdocs_negativevalues) {
  std::vector<ScoredDocument> docs;
  AddScoredDocs(docs, 1);
  AddScoredDocs(docs, 4);
  AddScoredDocs(docs, -9999);
  AddScoredDocs(docs, 3);

  sortAndFilterDocs(docs);

  ASSERT_THAT(docs.size(), Eq(3));
  ASSERT_THAT(docs[0].score, Eq(1));
  ASSERT_THAT(docs[1].score, Eq(3));
  ASSERT_THAT(docs[2].score, Eq(4));
}

// A predicate-formatter for asserting that two integers are mutually prime.
testing::AssertionResult CheckScoresBeforeAfter(
    const char* docs_expr, const char* expected_expr,
    const std::vector<ScoredDocument>& docs, std::vector<int> expected) {
  std::vector<int> actual_score;
  std::transform(docs.begin(), docs.end(), std::back_inserter(actual_score),
                 [](const ScoredDocument& scoredDocument) {
                   return scoredDocument.score;
                 });

  if (std::equal(actual_score.begin(), actual_score.end(), expected.begin(),
                 expected.end())) {
    return testing::AssertionSuccess();
  } else {
    std::vector<int> v = {1, 2, 3};
    fmt::print("{}\n", v);
    return testing::AssertionFailure() << fmt::format(
               "CheckScoresBeforeAfter() failed,\nExpected Output {} "
               "{},\nActual Output {} {}",
               expected, docs_expr, expected_expr, actual_score);
  }
}

TEST(sort_and_filter, Test1) {
  std::vector<ScoredDocument> docs;
  AddScoredDocs(docs, 1);
  AddScoredDocs(docs, 4);
  AddScoredDocs(docs, -9999);
  AddScoredDocs(docs, 3);

  sortAndFilterDocs(docs);

  std::vector<int> expected{1, 3, 4};
  EXPECT_PRED_FORMAT2(CheckScoresBeforeAfter, docs, expected);
}

//TEST(LogAnalyzerTest,
//     isValidLogFileName_with_empty_name_will_throw_invalid_valument_exception) {
//  ASSERT_THROW(logAnalyzer.isValidLogFileName(""), std::invalid_argument);
//}
//
//TEST(LogAnalyzerTest,
//     isValidLogFileName_with_too_short_name_return_false) {
//  ASSERT_FALSE(logAnalyzer.isValidLogFileName("name.log"));
//}
//
//TEST(LogAnalyzerTest,
//     isValidLogFileName_without_log_suffix_return_false) {
//  ASSERT_FALSE(logAnalyzer.isValidLogFileName("filename"));
//}
//
//TEST(LogAnalyzerTest,
//     isValidLogFileName_with_valid_lower_cased_return_false) {
//  ASSERT_TRUE(logAnalyzer.isValidLogFileName("systemfilename.log"));
//}
//
//
//int foo(int iRecordNum, int itype) {
//  int x = 0;
//  int y = 0;
//  while(iRecordNum > 0) {
//    if(itype == 0) {
//      x = y + 2;
//      break;
//    } else {
//      if(itype == 1) {
//        x = y + 10;
//      } else {
//        x = y + 20;
//      }
//    }
//    y++;
//    itype--;
//    iRecordNum -= 10;
//  }
//  return x;
//}
//
//
//TEST(DatabaseTest, should_write_to_database) {
//  accounts.createUser("foobar");
//  EXPECT_CALL(mock_database, put(Eq("foobar")));
//}
//
//TEST(DatabaseTest, should_create_users) {
//  accounts.createUser("foobar");
//  ASSERT_THAT(accounts.getUser("fooBar"), Ne(nullptr));
//}
//
//TEST(CaculatorTest, should_perform_addition) {
//  Calculator caculator{GetRoundingStrategy(), "unused",
//                       ENABLE_COSINE_FEATURE, 0.01, calculusEngine, false};
//  int result = caculator.caculate(getTestCaculation());
//  ASSERT_THAT(result, Eq(5));
//}
//
//TEST(CaculatorTest, should_perform_addition) {
//  Calculator caculator = getTestCalculator();
//  int result = caculator.caculate(Calculation(2, Operation::PLUS, 3)));
//  ASSERT_THAT(result, Eq(5));
//}
//@Test
//public void shouldPerformAddition() {
//  Calculator calculator = new Calculator(new RoundingStrategy(), "unused", ENABLE_COSINE_FEATURE, 0.01, calculusEngine, false);
//  int result = calculator.calculate(newTestCalculation());
//  assertThat(result).isEqualTo(5); // 这个数字从哪里来的?
//}
//
//class TransactionProcessor {
// public:
//  void displayTransactionResults(User user, Transaction transaction) {
//    ui.showMessage("You bought a " + transaction.getItemName());
//    if (user.getBalance() < LOW_BALANCE_THRESHOLD) {
//      ui.showMessage("Warning: your balance is low!");
//    }
//  }
//};
//
//TEST(TransactionProcessorTest, testDisplayTransactionResults) {
//  transactionProcessor.displayTransactionResults(
//      getUserWithBalance(LOW_BALANCE_THRESHOLD.plus(dollars(2))),
//      Transaction("Some Item", dollars(3)));
//  ASSERT_THAT(ui.getText, HasSubstr("You bought a Some Item"));
//  ASSERT_THAT(ui.getText, HasSubstr("your balance is low"));
//}
//
//TEST(TransactionProcessorTest, displayTransactionResults_should_showsItemName) {
//  transactionProcessor.displayTransactionResults(
//      User(),
//      Transaction("Some Item"));
//  ASSERT_THAT(ui.getText, HasSubstr("You bought a Some Item"));
//}
//
//TEST(TransactionProcessorTest, displayTransactionResults_should_showsLowBalanceWarning) {
//  transactionProcessor.displayTransactionResults(
//      getUserWithBalance(LOW_BALANCE_THRESHOLD.plus(dollars(2))),
//      Transaction("Some Item", dollars(3)));
//  ASSERT_THAT(ui.getText, HasSubstr("your balance is low"));
//}
//
//TEST(TransactionProcessorTest, transferFunds_should_move_money_between_accounts) {
//  // Given: 两个账户的初始余额为 $150 和 $20
//  Account account1 = newAccountWithBalance(usd(150));
//  Account account2 = newAccountWithBalance(usd(20));
//
//  // When: 将100美元从第一个帐户转到第二个帐户
//  bank.transferFunds(account1, account2, usd(100));
//
//  // Then: 新账户余额应反映这个转移
//  ASSERT_THAT(account1.getBalance(), Eq(usd(50)));
//  ASSERT_THAT(account2.getBalance(), Eq(usd(120)));
//}
//
//TEST(PoolTest, shouldTimeOutConnections) {
//  // Given two users
//  User user1 = newUser();
//  User user2 = newUser();
//
//  // And an empty connection pool with a 10-minute timeout Pool
//  pool = newPool(Duration.minutes(10));
//
//  // When connecting both users to the pool
//  pool.connect(user1);
//  pool.connect(user2);
//
//  // Then the pool should have two connections
//  ASSERT_THAT(pool.getConnections(), SizeIs(2));
//
//  // When waiting for 20 minutes
//  clock.advance(Duration.minutes(20));
//
//  // Then the pool should have no connections
//  ASSERT_THAT(pool.getConnections(), IsEmpty());
//
//  // And each user should be disconnected
//  ASSERT_THAT(user1.isConnected, IsFalse());
//  ASSERT_THAT(user2.isConnected, IsFalse());
//}

//TEST(NavigatorTest, shouldNavigateToAlbumsPage) {
//  std::string baseUrl = "http://photos.google.com/";
//  Navigator nav(baseUrl);
//  nav.goToAlbumPage();
//  ASSERT_THAT(nav.getCurrentUrl(), Eq("http://photos.google.com//albums"));
//}
//
//
////std::vector colors{"red", "green", "blue"};
////ASSERT_TRUE(std::find(colors.begin(), colors.end(), "red") != colors.end());  // 一把锤子创天下
////ASSERT_THAT(colors, Contains("red"));    // google mock matcher
//
//TEST(Forumtest, shouldAllowMultipleUsers) {
//  std::vector<User> users = createUsers(false, false);
//  Forum forum = createForumAndRegisterUsers(users);
//  validateForumAndUsers(forum, users);
//}
//
//TEST(Forumtest, shouldNotAllowBannedUsers) {
//  std::vector<User> users = createUsers(true);
//  Forum forum = createForumAndRegisterUsers(users);
//  validateForumAndUsers(forum, users);
//}
//// Lots more tests...
//std::vector<User> createUsers(std::initializer_list<bool> banned) {
//  std::vector<User> users{};
//  for (auto isBanned : banned) {
//    users.emplace(isBanned ? State::BANNED : State::NORMAL);
//  }
//  return users;
//}
//Forum createForumAndRegisterUsers(const std::vector<User>& users) {
//  Forum forum{};
//  for (auto& user : users) {
//    ASSERT_NO_THROW(forum.register(user));
//  }
//  return forum;
//}
//void validateForumAndUsers(Forum& forum, const std::vector<User>& users) {
//  ASSERT_TRUE(forum.isReachable());
//  for (auto& user : users) {
//    if(user.getState() == State::BANNED) {
//      ASSERT_THAT(forum.hasRegisteredUser(user), IsTrue());
//    }
//  }
//}
//
//TEST(Forumtest, shouldAllowMultipleUsers) {
//  User user1{State::NORMAL};
//  User user2{State::NORMAL};
//  Forum forum{};
//  ASSERT_NO_THROW(forum.register(user1));
//  ASSERT_NO_THROW(forum.register(user2));
//  ASSERT_THAT(forum.hasRegisteredUser(user1), IsTrue());
//  ASSERT_THAT(forum.hasRegisteredUser(user2), IsTrue());
//}
//
//TEST(Forumtest, shouldNotAllowBannedUsers) {
//  User user{State::BANNED};
//  Forum forum{};
//  ASSERT_NO_THROW(forum.register(user));
//  ASSERT_THAT(forum.hasRegisteredUser(user), IsFalse());
//}
//// Lots more tests...
//
//class ForumTest : public Test {
// protected:
//  Account ACCOUNT_1{State::OPEN, 50};
//  Account ACCOUNT_2{State::CLOSED};
//  Item ITEM{"Cheeseburger", 100};
//};
//
//
//
//
//TEST(ForumTest, canBuyItem_returnsFalseForClosedAccounts) {
//  ASSERT_FALSE(store.canBuyItem(ITEM, ACCOUNT_1));
//}
//
//TEST(ForumTest, canBuyItem_returnsFalseWhenBalanceInsufficient) {
//  ASSERT_FALSE(store.canBuyItem(ITEM, ACCOUNT_2));
//}
//
//class ForumTest : public Test {
// protected:
//  Account CLOSED_ACCOUNT{State::OPEN, 50};
//  Account ACCOUNT_WITH_LOW_BALANCE{State::CLOSED};
//  Item ITEM{"Cheeseburger", 100};
//};
//
//TEST(ForumTest, canBuyItem_returnsFalseForClosedAccounts) {
//  ASSERT_FALSE(store.canBuyItem(ITEM, CLOSED_ACCOUNT));
//}
//
//TEST(ForumTest, canBuyItem_returnsFalseWhenBalanceInsufficient) {
//  ASSERT_FALSE(store.canBuyItem(ITEM, ACCOUNT_WITH_LOW_BALANCE));
//}
//
//class NameServiceTest : public Test {
// protected:
//  std::unique_ptr<NameService> nameService;
//  std::unique_ptr<UserStore> userStore;
//  void SetUp() override {
//    nameService.reset(new NameService());
//    nameService->set("user1", "Donald Knuth");
//    userStore.reset(new UserStore(nameService));
//  }
//};
//
//// [... hundreds of lines of tests ...]
//
//
//TEST(NameServiceTest, shouldReturnNameFromService) {
//  auto user = userStore.get("user1");
//  ASSERT_THAT(user.getName(), Eq("Donald Knuth"));
//}


//TEST_CASE( "An MRU list acts like a stack, "
//"but duplicate entries replace existing ones" ) {
//MRUList<std::string> list; SECTION( “...?” ) {
//list.add("item1"); }
//}
//
//
//TEST(StringListTest, an_default_account_list_should_be_empty) {
//  StringList list;
//  ASSERT_TRUE(list.empty());
//  ASSERT_EQ(list.size(), 0);
//
//}
//
//TEST(StringListTest, an_account_list_after_add_item_should_not_be_empty) {
//  StringList list;
//  list.add("item");
//
//  ASSERT_FALSE(list.empty);
//  ASSERT_EQ(list.size(), 1);
//}

//class Raffle {
// private:
//  std::vector<int> tickets;
// public:
//  Raffle() : tickets{globalData.getTicketsList()} {
//  }
//
//  int ticketCount() const {
//    return tickets.size();
//  }
//};

//class Raffle {
// private:
//  std::vector<int> tickets;
// public:
//  explicit Raffle(std::vector<int> tickets) : tickets{std::move(tickets)} {
//  }
//
//  int ticketCount() const {
//    return tickets.size();
//  }
//};
//
//TEST(RaffleTest, shouldHaveFiveTickets) {
//  Raffle testedRaffle{std::vector<int>{1, 2, 3, 4, 5}};
//  ASSERT_EQ(5, testedRaffle.ticketCount());
//}


//class Raffle {
//  std::vector<int> tickets;
// public:
//  Raffle() {
//    tickets = CreateTicket();
//  }
//
//  int ticketCount() const {
//    return tickets.size();
//  }
//
//  virtual std::vector<int> CreateTicket() {
//    return globalData.getTicketsList();
//  }
//};
//
//class FiveTicketRaffle : public Raffle {
// public:
//  std::vector<int> CreateTicket() final {
//    return {1, 2, 3, 4, 5};
//  }
//};
//
//TEST(RaffleTest, shouldHaveFiveTickets) {
//  FiveTicketRaffle testedRaffle;
//  ASSERT_EQ(5, testedRaffle.ticketCount());
//}

//class TicketsFactory{
//  int numberOfTickets;
// public:
//  explicit TicketsFactory(int num) : numberOfTickets(num) {
//
//  }
//  std::vector<int> createTickets() const{
//    std::vector<int> tickets;
//    for(int i = 0; i < numberOfTickets; ++i) {
//      tickets.push_back(i);
//    }
//    return tickets;
//  }
//};
//
//class Raffle {
// private:
//  std::vector<int> tickets;
// public:
//  explicit Raffle(const TicketsFactory& factory) : tickets{factory.createTickets()} {
//  }
//
//  int ticketCount() const {
//    return tickets.size();
//  }
//};
//
//TEST(RaffleTest, shouldHaveFiveTickets) {
//  Raffle testedRaffle{TicketsFactory{5}};
//  ASSERT_EQ(5, testedRaffle.ticketCount());
//}
//
//#include <fstream>
//std::vector<Payment> readPaymentFile(std::string filename) {
//  std::ifstream paymentFile(std::move(filename), std::ifstream::in);
//
//  std::string line;
//  while(!paymentFile.eof()) {
//    std::getline(paymentFile, line);
//    //剩下处理文件解析的事情
//  }
//}
//
//class PaymentFile {
// public:
//  bool  hasMoreLines() const;
//  std::string readLine();
//};
//
//std::vector<Payment> readPaymentFile(PaymentFile& file) {
//
//  while(file.hasMoreLines()) {
//    auto line = file.readLine();
//    //剩下处理文件解析的事情
//  }
//}
//
//std::vector<Payment> readPaymentFile(std::istream& inputStream) {
//  while(!inputStream.eof()) {
//    std::string line;
//    std::getline(inputStream, line);
//    //剩下处理文件解析的事情
//  }
//}
//
//#include <sstream>
//TEST(PaymentTest, should_parse_line_into_payment) {
//  std::stringstream test_stream;
//  test_stream << "91243878784;1000.00;20210610\n";
//  auto payment = readPaymentFile(test_stream);
//  ASSERT_THAT(payment, Eq(Payment{91243878784, 1000.00, 20210610}));
//
//}
//
//
//
//void DispatchPayment(Payment payment) {
//  auto now = std::chrono::high_resolution_clock::now();
//  if(now < payment.DueDate) {
//    ReceiveOnTimePayment(payment);
//  } else {
//    // 处理迟到的和可能错误的付款
//  }
//
//  // 后续其他处理
//}
//
//using Clock = std::chrono::high_resolution_clock;
//using TimePoint = std::chrono::time_point<Clock>;
//
//class TimeSource {
// public:
//  virtual TimePoint now() {
//    Clock::now();
//  }
//};
//
//class FakeTimeSource {
//  TimePoint current;
// public:
//  FakeTimeSource(TimePoint &timePoint) : current(timePoint) {
//
//  }
//  TimePoint now() {
//    return current;
//  }
//};
//
//class PaymentProcessor {
// private:
//  CreditCardService* creditCardService;
//  ...
//public:
//  PaymentProcessor (CreditCardService* service) : creditCardService(service) {
//  }
//
//  bool makePayment(CreditCard& creditCard, Money& amount) {
//    if (creditCard.isExpired()) {
//      return false;
//    }
//    bool success =
//        creditCardService.chargeCreditCard(creditCard, amount);
//    return success;
//  }
//};
//
//class TestDoubleCreditCardService : public CreditCardService {
// public:
//  bool chargeCreditCard(CreditCard creditCard, Money amount) {
//    return true;
//  }
//}
//
//TEST(PaymentProcessorTest, cardIsExpired_returnFalse) {
//  TestDoubleCreditCardService creditCardService;
//  PaymentProcessor paymentProcessor(&creditCardService);
//  ASSERT_FALSE(paymentProcessor.mayPayment(EXPIRED_CARD, AMOUNT));
//}
//
//
//
//
//
//class SystemClock {
//  static TimeSource* timeSource;
// public:
//  static void setTimeSource(TimeSource& timeSource) {
//    SystemClock::timeSource = &timeSource;
//  }
//  static TimePoint now() {
//    return timeSource->now();
//  }
//};
//
//void DispatchPayment(Payment payment) {
//  auto now = SystemClock::now();
//  if(now < payment.DueDate) {
//    ReceiveOnTimePayment(payment);
//  } else {
//    // 处理迟到的和可能错误的付款
//  }
//
//  // 后续其他处理
//}
//
//
// 传入由模拟框架创建的测试替身



