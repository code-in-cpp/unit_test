//
// Created by yonggang zhao on 2021/5/29.
//

#ifndef UNIT_TEST_SCOREDDOCUMENT_H
#define UNIT_TEST_SCOREDDOCUMENT_H

#include <string>
#include <vector>

struct ScoredDocument {
  std::string doc;
  int score;
};

void sortAndFilterDocs(std::vector<ScoredDocument>& docs);

#endif  // UNIT_TEST_SCOREDDOCUMENT_H
