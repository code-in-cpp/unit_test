//
// Created by yonggang zhao on 2021/5/29.
//

#include "scored_document.h"

#include <algorithm>
void sortAndFilterDocs(std::vector<ScoredDocument>& docs) {
  if(!docs.empty()) {
    docs.erase(std::remove_if(docs.begin(), docs.end(),
                              [](const ScoredDocument& scoredDocument) {
                                return scoredDocument.score < 0;
                              }));
    std::sort(docs.begin(), docs.end(), [](const ScoredDocument& lhs, const ScoredDocument& rhs) {
      return lhs.score < rhs.score;
    });
  } else {
      docs.emplace_back(ScoredDocument{"test", 1});
  }
}
